import React from 'react'
import { useSelector} from 'react-redux'


const ShoppingCart = () => {

    const productsInCart= useSelector(store=> store.product.productsInCart)
    const [dataInCart,SetDataInCart] = React.useState(productsInCart)
  
    
    return (
        <div className="container mt-5">

            <h3>Shopping Cart</h3>
            <ul>
                {
                     dataInCart.map( (item =>
                            <li key={item.SKU}>{item.NAME} {item.SKU }</li>
                    ) )
                }
            </ul>
            
        </div>
    )
}

export default ShoppingCart
