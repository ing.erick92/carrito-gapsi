import React from 'react'
import axios from 'axios'
import logo from '../assets/img/logo.png'

import {
    BrowserRouter as Router,
    Link
  } from "react-router-dom";

const Home = () => {
    const [textWelcome, settextWelcome] = React.useState('');
    const [versionApp, setversionApp] = React.useState('');

    React.useEffect( ()=> {
        
      const getServText = async () => {
          
            try {
                const response = await axios.post('https://node-red-nxdup.mybluemix.net/visitor');
                settextWelcome(response.data.data.welcome)
                setversionApp(response.data.data.version)
            } catch (error) {
                console.log(error.message)
            }
          }

          getServText()
          
    },[])
    return (
        <div className="container">
            <div className="row d-flex justify-content-center mt-5">
              <div className="col-md-5">
                <div className="card d-flex justify-content-center">
                    <figure className="figure">
                        <img src={logo} className="card-img-top" alt="logo Gapsi" />
                    </figure>
                    <div className="card-body">
                        <h5 className="card-title">Bienvenido, {textWelcome}</h5>
                        <Link className="btn btn-primary mt-5" to="/products">Continuar</Link>
                    </div>
                </div>
                </div>
            </div>    

            <div className="row">
                <div className="hr mt-5 text-end">
                    Versión - {versionApp}
                </div>
            </div>        
        </div>
    )
}

export default Home
