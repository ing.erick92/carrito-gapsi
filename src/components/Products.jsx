import React from 'react'

// Se importa useDispat y useSelect para poder utilizar los metodos y varibles de del archivo ReduxProdus
import { useDispatch,useSelector} from 'react-redux'
import{getProductsAction,addProductInCart} from '../redux/ReduxProducts'
import ShoppingCart from './ShoppingCart'

const Products = () => {

    const dispatch= useDispatch()
    const products= useSelector(store=> store.product.products)
    const category = useSelector(store=>store.product.category)
    

    React.useEffect(() => {
         dispatch(getProductsAction('reloj',1))
      },[]);

    
      return (
        <div className="container">
            <ShoppingCart/>

            <div className="row">
                <div className="col-md-2">
                    <h4>Categorias</h4>
                    <button value="videojuego" onClick={ (e)=>dispatch(getProductsAction(e.target.value,1))} type="button" className="btn mt-3 btn-primary">Videojuegos</button>
                    <button value="camisa" onClick={ (e)=>dispatch(getProductsAction(e.target.value,1))} type="button" className="btn mt-3 btn-primary">Camisas</button>
                    <button value="reloj" onClick={ (e)=>dispatch(getProductsAction(e.target.value,1))} type="button" className="btn mt-3 btn-primary active">Relojes</button>
                </div>

                <div className="col-md-10">
                {
                    products && <h2>Lista de Productos</h2> 
                }
                  <div className="row">
                {
                   products.map( (item=>
                    
                    <div key={item.NAME+item.SKU} className="card col-md-3" >
                    <img src={item.IMAGE} className="card-img-top" alt={item.NAME+item.SKU}/>
                    <div className="card-body">
                        <h5 className="card-title">{item.NAME+" "+item.SKU}</h5>
                        <p className="card-text">{item.DESCRIPTION}</p>
                        <a onClick={ ()=>dispatch(addProductInCart(item)) } className="btn btn-primary">Agregar al Carrito</a>
                    </div>
                    </div>
                        ) 
                    )
                }
                 </div>
              </div>
            </div>
            

        </div>
    )
}

export default Products
