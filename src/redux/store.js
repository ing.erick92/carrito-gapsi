import {createStore, combineReducers,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import productReducer from './ReduxProducts'

const rootReducer = combineReducers({
    product: productReducer
})

//const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore(){
    const store = createStore( rootReducer, applyMiddleware(thunk)) 
    return store
}