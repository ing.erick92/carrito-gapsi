import axios from 'axios'

const dataInicial = {
    category:'',
    products: [],
    productsInCart:[]
}


const GETPRODUCTS = 'GETPRODUCTS'
const GETPRODUCTSINCART = 'GETPRODUCTSINCART'

export default function productReducer (state = dataInicial, action){

    switch(action.type){
        case GETPRODUCTS:
            return {...state, ...action.payload }
        case GETPRODUCTSINCART:
                return {...state, ...action.payload.productsInCart }
        default: 
            return {...state}
    }

}

export const getProductsAction = (cat,pag) => async(dispatch) => {
    //alert("se ejecuta la funciona getProducts del redux")
    try {
        
         const resp = await axios( `https://node-red-nxdup.mybluemix.net/productos/${cat}/${pag}`);
         console.log("response de service getProducts" +resp);
         dispatch({
             type:GETPRODUCTS,
             payload: {
                 products:resp.data.data.products,
                 category:cat
             }
         })
        
    } catch (error) {
        console.log(error)
      
    }
}

export const addProductInCart = (items) => (dispatch,getState) => {

    const add = getState().product.productsInCart.push(items) ;

    dispatch({
        type:GETPRODUCTSINCART,
        payload: {
            productsInCart: {...add}
        }
    })
     
}